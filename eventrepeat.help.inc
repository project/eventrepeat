<?php

/**
 * @file
 *
 * Event Repeats and repeating events to the event module.
 */

/**
 * Prints the user help page for repeating events.
 */
function eventrepeat_user_help_page() {
  $output = _eventrepeat_user_help_content();
  return $output;
}

/**
 * Creates help based on current configuration.
 *
 * @param $admin Set to TRUE to show all advanced inforamtion
 * @return translated html text
 *
 * @ingroup eventrepeat_support
 */
function _eventrepeat_user_help_content($admin = FALSE) {
  $show_advanced = variable_get('eventrepeat_showadvanced', array());

  $output = '';
  $output .= '<p>' . t('To create a repeat sequence expand the "Repeat" fieldset and choose the following:') . "</p>\n";
  $output .= t('<ol><li>Set "Repeat type" to the type of repeating pattern you wish to create</li><li>Set either "Repeat end date" or "count" to determine the how many repeating events will be created (you can only set one of these parameters). If you want the pattern to be indefinite, then leave both of these settings empty.</li><li>Expand the "Advanced" fieldset to further define your repeat pattern Note that if this site has not been configured to use any advanced options, this control may not be present.</li></ol>');
  $output .= '<p>' . t('After you\'re created your sequence, you can delete occurrences and create "exceptions" to the repeat sequence by editing the individual events in the calendar. Just make sure you choose the "This occurrence only" option.') . "</p>\n";
  $output .= '<p>' . t('Please note: You can\'t create repeat events in the past - if you set a sequence starting in the past, it will begin to render on the current date (or possibly the day before)') . "</p>\n";

  // Add help for each advanced option enabled
  $adv_output = '';
  if ($admin || $show_advanced['eventrepeat_INTERVAL']) {
    $adv_output .= '<dd><em>' . t('Interval') . '</em></dd>';
    $adv_output .= '<dd>' . t('Select the frequency of repeat: 1 = every, 2 = every other, 3 = every 3rd, etc.') . '</dd>';
  }
  if ($admin || $show_advanced['eventrepeat_BYDAY']) {
    $adv_output .= '<dd><em>' . t('By day of the week') . '</em></dd>';
    $adv_output .= '<dd>' . t('This is a giant list of the seven days of the week, followed by additional options for more complex repeat patterns. Determines what day(s) of the week/month this event repeats on by day of the week. Scroll down to get options such as "3rd from Last Friday" or "1st Saturday".') . '</dd>';
  }
  if ($admin || $show_advanced['eventrepeat_BYWEEKNO']) {
    $adv_output .= '<dd><em>' . t('By month') . '</em></dd>';
    $adv_output .= '<dd>' . t('This is a multiple-select box of the 12 month names. Select what month(s) of the year this event repeats on') . '</dd>';
  }
  if ($admin || $show_advanced['eventrepeat_BYMONTH']) {
    $adv_output .= '<dd><em>' . t('By days of the month') . '</em></dd>';
    $adv_output .= '<dd>' . t('This is a multiple-select box of numbers from 1 to 31 and -1 to -31. Determines what day(s) of the month this event repeats on (the actual day number in the month). Negative numbers count from the end of the month.') . '</dd>';
  }
  if ($admin || $show_advanced['eventrepeat_BYMONTHDAY']) {
    $adv_output .= '<dd><em>' . t('By days of the year') . '</em></dd>';
    $adv_output .= '<dd>' . t('This is a multiple-select box of numbers from 1 to 366 and -1 to -366. Determines what day(s) of the year this event repeats on. Negative numbers count from the end of the year.') . '</dd>';
  }
  if ($admin || $show_advanced['eventrepeat_BYYEARDAY']) {
    $adv_output .= '<dd><em>' . t('By number of the week') . '</em></dd>';
    $adv_output .= '<dd>' . t('This is a multiple-select box of numbers from 1 to 54 and -1 to -54. Selects what week(s) of the year this event repeats on. Negative numbers count from the end of the year.') . '</dd>';
  }

  // If we have at least one of the advanced options, add the advanced section
  if ($adv_output) {
    $output .= '<p>' . t('Notes on using the advanced options:') . '</p>';
    $output .= '<dl>' . $adv_output . '</dl>';
    $output .= '<p>' . t('Except for the count parameter, all other parameters operate as follows: Multiple selections within the same parameter use an OR comparison for determining the pattern (ex. Monday OR Tuesday OR Wednesday). Choosing multiple parameters uses an AND comparison between the parameters (ex. on Monday AND in March).So, setting the days parameter to Monday, Wednesday--and the month parameter to July, August would result in this comparison logic: Occurs on (Monday OR Wednesday) AND (July OR August).') . "</p>\n";
  }

  return $output;
}
