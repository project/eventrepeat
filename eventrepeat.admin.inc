<?php

/**
 * @file
 *
 * Event Repeats and repeating events to the event module.
 */

/**
 * Creates and displays the administrative configuration form.
 */
function eventrepeat_admin_settings() {
  $form = array();

  // Create a link to the content types admin page
  //$form['content_types'] = array(
  //  '#value' => t(''),
  //);

  // Event display
  $form['eventrepeat_display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Event Display'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['eventrepeat_display']['eventrepeat_title_tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Title tag'),
    '#default_value' => variable_get('eventrepeat_title_tag', '[R]'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t('Enter a tag that will be prepended to all events in a repeat sequence. Leave this blank if you don\'t want any indicator to display.')
  );

  // create a fieldset to turn on / off the advanced controls
  $form['eventrepeat_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Controls'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $advanced_options = array(
    'eventrepeat_INTERVAL' => t('Interval'),
    'eventrepeat_BYDAY' => t('By day of the week'),
    'eventrepeat_BYMONTH' => t('By month'),
    'eventrepeat_BYMONTHDAY' => t('By days of the month'),
    'eventrepeat_BYYEARDAY' => t('By days of the year'),
    'eventrepeat_BYWEEKNO' => t('By number of the week'),
  );
  $form['eventrepeat_advanced']['eventrepeat_showadvanced'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show the following controls'),
    '#default_value' => variable_get('eventrepeat_showadvanced', array()),
    '#options' => $advanced_options,
    '#description' => t('These options will appear in the advanced fieldset of the repeat fieldset on nodes that use repeat events.'),
  );

  // default edit type
  $form['eventrepeat_behavior'] = array(
    '#type' => 'fieldset',
    '#title' => t('Repeat Sequence Behavior'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['eventrepeat_behavior']['eventrepeat_initial_render'] = array(
    '#type' => 'textfield',
    '#title' => t('Initial render period'),
    '#default_value' => variable_get('eventrepeat_initial_render', 90),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Initial period of time for which a repeat sequence is rendered upon it\'s creation--also the number of days from the current date that repeating nodes are automatically updated (events outside this range will only be rendered upon first viewing of a calendar period that contains them, up to the rendering support period). Default value is 90 days. Maximum allowed value is 730 days')
  );
  $form['eventrepeat_behavior']['eventrepeat_render_support'] = array(
    '#type' => 'textfield',
    '#title' => t('Render support period'),
    '#default_value' => variable_get('eventrepeat_render_support', 2000),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('Number of days from current date that repeat rendering is supported. Default is 2000 days, and it\'s recommended that this value be kept. Maximum allowed value is 10000. Note that the database is refreshed with this many rows from todays date on the first cron run or if cron has not been run in a while. High values could, potentially, cause a cron or database timeout.')
  );
  $form['eventrepeat_behavior']['eventrepeat_single_edit_in_sequence'] = array(
    '#type' => 'checkbox',
    '#title' => t('Leave individual edits in sequence'),
    '#default_value' => variable_get('eventrepeat_single_edit_in_sequence', FALSE),
    '#description' => t('If selected, individually edited nodes will remain part of their repeat sequence<br />WARNING: Subsequent mass edits involving the individually edited node will overwrite the old data!')
  );
  $det_options = array(
    'this' => t('Only the event being edited'),
    'future' => t('The event being edited and all future occurrences'),
    'all' => t('All occurrences of the event')
  );
  $form['eventrepeat_behavior']['eventrepeat_default_edit_type'] = array(
    '#type' => 'radios',
    '#title' => t('By default, apply edits to'),
    '#default_value' => variable_get('eventrepeat_default_edit_type', 'future'),
    '#options' => $det_options,
    '#description' => t('Sets the default selection for how repeating events are edited. "All occurrences" will make the default to edit all events in the sequence after today\'s date. "The event being edited and all future occurrences" will make the default to edit events from the date of the selected node forward. "Only the event being edited" will make editing the one event the default and may remove the event from the sequence depending on the setting above.'),
  );

  // order of date elements
  $form['eventrepeat_dateform'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Display'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $order_options = array(
    'euro' => t('European (day month year)'),
    'us' => t('American (month day year)'),
  );
  $form['eventrepeat_dateform']['eventrepeat_dateform_order'] = array(
    '#type' => 'radios',
    '#title' => t('Show the form elements in this order:'),
    '#default_value' => variable_get('eventrepeat_dateform_order', 'euro'),
    '#options' => $order_options,
    '#description' => t('This will determine the order of date elements in the node form for the repeat end and exception controls.'),
  );

  // special user switching
  $form['user_switching'] = array(
    '#type' => 'fieldset',
    '#title' => t('User switching'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  $form['user_switching']['eventrepeat_special_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Render user'),
    '#default_value' => variable_get('eventrepeat_special_user', ''),
    '#autocomplete_path' => 'user/autocomplete',
    '#size' => 32,
    '#maxlength' => 64,
    '#description' => t('When rendering new nodes for your repeat sequences, Drupal will switch to the user you chose here. This setting is only important when newly created repeated nodes are acted upon by other modules that do check for special permissions. Enter valid username.')
  );

  return system_settings_form($form);
}


/**
 * Validation function for the administrative settings form.
 *
 * @param array $form
 *   The definition of the form.
 * @param array $form_state
 *   The values from the submitted form.
 */
function eventrepeat_admin_settings_validate($form, &$form_state) {
  if (!(is_numeric($form_state['values']['eventrepeat_initial_render']) && (int) $form_state['values']['eventrepeat_initial_render'] <= 730)) {
    form_set_error('eventrepeat_initial_render', t('The initial render period must be a number that is less than or equal to 730.'));
  }

  if (!(is_numeric($form_state['values']['eventrepeat_render_support']) && (int) $form_state['values']['eventrepeat_render_support'] <= 10000)) {
    form_set_error('eventrepeat_render_support', t('The render support period must be a number less than or equal to 10,000.'));
  }

  if (drupal_strlen($form_state['values']['eventrepeat_special_user']) > 0) {
    $account = user_load(array('name' => $form_state['values']['eventrepeat_special_user']));
    if (!$account->uid) {
      form_set_error('eventrepeat_special_user', t('Not a valid username.'));
    }
  }
}
