About
-----

Event Repeat enables the creation of repeating event patterns for node types
that are event-enabled. It does this by creating one or more copies of the
original event node.

Note: This module deals with automatic creation, editing, and deletion of nodes.
It is highly recommended that you run this module on a test site, first, to make
sure it does for you what you think it does!

Features
--------

* Basic repeat types of daily, weekly, monthly or yearly.
* Repeating events limited by count or end date.
* Optional advanced controls to add repeats by:
  - every other, every third, etc.
  - day of week per month
  - xth day of week per month
  - xth from last day of week per month
  - month
  - number of the day per month
  - number of the day in the year
  - week number

Contact
-------

Log bug reports and feature requests at
http://drupal.org/project/issues/eventrepeat

Requirements
------------

This module requires Drupal 6 or higher.
This module requires the Drupal 6 verion of the Event module.
This module requires cron, but it can be run manually (see below).

Installation
------------

For installation instructions, see INSTALL.txt in this directory.

Repeat Patterns
---------------

Eventrepeat's pattern creation was largely modeled on the iCal RRULE
specification. At this time, it should support all RRULE parameters, with the
following exceptions:
* Recurrence periods less than DAILY
* BYDAY declarations greater than 5 and less than -5 (ex. 20th Monday of the
  year is not supported). Other similar patterns can be built that should
  approximate this functionality.
* BYSETPOS parameter

Cron
----

For best results, cron should be enabled to hit cron.php on your site.

For more information on setting up cron, see http://drupal.org/cron.

During the installation process, eventrepeat_cron() will be called and up to
2000 records will be inserted into the event_repeat_calendar_map table.
Future calls to cron.php will create additional entries in this table, so that
the table contains 2000 entries, one for each day out to 2000 days from today.
Note that this assumes that you have not reduced the Render support period.

Calling cron.php will also delete any records in event_repeat_calendar_map
before yesterday in an attempt to keep this table from getting too large.

Settings
--------

* Event Display
  + Title tag:
    Enter a tag that will be prepended to all events in a repeat sequence. Leave
    this blank if you don't want any indicator to display.

* Advanced Control
  + Show the following controls:
    These options will appear in the advanced fieldset of the repeat fieldset on
    nodes that use repeat events. Note that enabling all of these will confuse
    some users. Each option is described here:
    - Interval:
      Frequency of repeat: 1 = every, 2 = every other, 3 = every 3rd, etc.
    - By day of the week:
      This is a giant list of the seven days of the week, followed by additional
      options for more complex repeat patterns. Determines what day(s) of the
      week/month this event repeats on by day of the week. Scroll down to get
      options such as "3rd from Last Friday" or "1st Saturday".
    - By month:
      This is a multiple-select box of the 12 month names. Selects what month(s)
      of the year this event repeats on
    - By days of the month:
      This is a multiple-select box of numbers from 1 to 31 and -1 to -31.
      Determines what day(s) of the month this event repeats on (the actual day
      number in the month). Negative numbers count from the end of the month.
    - By days of the year:
      This is a multiple-select box of numbers from 1 to 366 and -1 to -366.
      Determines what day(s) of the year this event repeats on. Negative numbers
      count from the end of the year.
    - By number of the week:
      This is a multiple-select box of numbers from 1 to 54 and -1 to -54.
      Selects what week(s) of the year this event repeats on. Negative numbers
      count from the end of the year.

* Repeat Sequence Behavior
  + Initial render period:
    Initial period of time for which a repeat sequence is rendered upon its
    creation - also the number of days from the current date that repeating
    nodes are automatically updated (events outside this range will only be
    rendered upon first viewing of a calendar period that contains them, up to
    the rendering support period). Default value is 90 days. Maximum allowed
    value is 730 days.
  + Render support period:
    Number of days from current date that repeat rendering is supported. Default
    is 2000 days, and it's recommended that this value be kept. Maximum allowed
    value is 10000. Note that the database is refreshed with this many rows from
    today's date on the first cron run or if cron has not been run in a while.
    High values could, potentially, cause a cron or database timeout. (See the
    section on cron below.)
  + Leave individual edits in sequence:
    If selected, individually edited nodes will remain part of their repeat
    sequence. WARNING: Subsequent mass edits involving the individually edited
    node will overwrite the old data!
  + By default, apply edits to:
    Sets the default selection for how repeating events are edited. "All
    occurrences" will make the default to edit all events in the sequence after
    today's date. "The event being edited and all future occurrences" will make
    the default to edit events from the date of the selected node forward. "Only
    the event being edited" will make editing the one event the default and may
    remove the event from the sequence depending on the setting above.

* Form Display
  + Show the form elements in this order:
    This will determine the order of date elements in the node form for the
    repeat end and exception controls. This essentially localizes the date form
    elements for repeat event. Hopefully this will be taken care of by a more
    centralized setting in the future.

Credits
---------------

Written by Chad Phillips (hummonk: http://drupal.org/user/22079)
Previously maintained by Sean B. Fuller (seanbfuller:
http://drupal.org/user/25382)
Previously maintained by Robert Middleswarth (rmiddle:
http://drupal.org/user/192183)
Currently maintained by Jason Flatt (oadaeh: http://drupal.org/user/4649)
